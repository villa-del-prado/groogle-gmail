package com.puravida.groogle

import com.puravida.groogle.impl.GroovyGmailService
import groovy.transform.CompileStatic

@CompileStatic
class GmailServiceBuilder {

    static GmailService build(){
        new GroovyGmailService()
    }
}
